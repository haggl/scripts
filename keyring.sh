#!/bin/sh -e
# @author Sebastian Neuser


# ---  C O N S T A N T S  --- #

# Device settings
DEFAULT_DEVICE_NAME=keyring
DEFAULT_DEVICE=/dev/$DEFAULT_DEVICE_NAME
DEVICE_MOUNT_POINT=/mnt/$DEFAULT_DEVICE_NAME

# LUKS container settings
CONTAINER_NAME=chest
CONTAINER_PATH=$DEVICE_MOUNT_POINT/$CONTAINER_NAME

SCRIPT_NAME=$(basename "$0")



# ---  S U B R O U T I N E S  --- #

lock_container() {
    echo "Locking $CONTAINER_PATH"
    vault.sh -Ccn $CONTAINER_PATH
}

mount_device() {
    device=${1:-$DEFAULT_DEVICE}
    echo "Mounting $device on $DEVICE_MOUNT_POINT"
    options="uid=$(id -u),gid=$(id -g),fmask=137,dmask=027"
    sudo mount -o $options "$device" $DEVICE_MOUNT_POINT || :
    unset device
}

print_help_and_exit() {
    cat << EOM
usage: $SCRIPT_NAME [-h]
       $SCRIPT_NAME  -S <size>
       $SCRIPT_NAME  -s <device>
       $SCRIPT_NAME <-M | -m> <partition>
       $SCRIPT_NAME <-U | -c | -o | -u>

options:
  -M    mount device and unlock LUKS volumes
  -S    setup LUKS container and mount point
  -U    lock LUKS volumes and unmount device
  -c    lock LUKS volumes
  -h    show this help messag
  -m    mount
  -o    unlock LUKS volumes
  -s    setup udev rules and mount points for <device>
  -u    unmount

parameters:
  device        '/dev/sda'  / '/dev/sdb'  ...
  partition     '/dev/sda1' / '/dev/sdb3' ...
  size          container size in MiB
EOM
    exit
}

setup_container() {
    vault.sh -Icn $CONTAINER_PATH "$1"
    vault.sh -Mn  $CONTAINER_NAME
}

setup_device() {
    vault.sh -Dn $DEFAULT_DEVICE_NAME "$1"
    vault.sh -Mn $DEFAULT_DEVICE_NAME
}

unlock_container() {
    echo "Unlocking container $CONTAINER_PATH"
    vault.sh -Ocn $CONTAINER_PATH
}

unmount_device() {
    echo "Unmounting keyring"
    sudo umount $DEVICE_MOUNT_POINT
}



# ---  M A I N   S C R I P T  --- #

if [ -z "$1" ]; then
    print_help_and_exit
fi

while getopts ":M:S:Uchm:os:u" option; do
    case $option in
        M) mount_device "$OPTARG"; unlock_container ;;
        S) mount_device; setup_container "$OPTARG" ;;
        U) lock_container; unmount_device ;;
        c) lock_container ;;
        m) mount_device "$OPTARG" ;;
        o) unlock_container ;;
        s) setup_device "$OPTARG" ;;
        u) unmount_device ;;
        :) case "$OPTARG" in
               M) mount_device; unlock_container ;;
               m) mount_device ;;
               *) print_help_and_exit ;;
           esac ;;
        *) print_help_and_exit
    esac
done
shift $((OPTIND - 1))
