#!/usr/bin/env python3
"""A little helper script for switching between screen, peripheral and networking setups.
"""

from argparse import ArgumentParser
from os.path import expanduser
from socket import gethostname
from subprocess import CalledProcessError, Popen, run
from sys import exit as sys_exit

from toml import load


def parse_cmdline():
    """Parses command line arguments given to the script.

    Returns:
        the command line arguments parsed by argparse
    """
    parser = ArgumentParser(description=globals()['__doc__'])
    parser.add_argument('--flip', '-f', action='store_true',
                        help='flip primary and secondary screen')
    parser.add_argument('location', help='name of a workstation configuration')
    return parser.parse_args()

def setup_peripherals(config: dict):
    """Performs various peripheral device setup tasks.
    """
    if 'wifi' in config:
        run(['rfkill', 'unblock' if config['wifi'] else 'block', 'wlan'], check=True)
    if 'mouse' in config:
        run(['invert_mouse_wheel.py', config['mouse']], check=True)
    if 'keyboard' in config:
        try:
            run(['xmodmap', expanduser('~/.Xmodmap')], check=True)
        except CalledProcessError:
            print('Warning: xmodmap failed')
    if 'wifi' in config:
        run(['rfkill', 'unblock' if config['wifi'] else 'block', 'wlan'], check=True)

def setup_screens(config: dict[str, str], flip=False):
    """Calls xrandr to setup screens according to specified configuration.
    """
    def xrandr(output: str, args: list[str]):
        run(['xrandr', '--output', output] + args, check=True)
    mode = ['--mode', config['resolution']] if 'resolution' in config else ['--auto']
    primary = config['primary']
    secondary = config['secondary'] if 'secondary' in config else None
    if secondary and flip:
        primary, secondary = (secondary, primary)
    xrandr(primary, ['--primary'] + mode)
    if secondary:
        xrandr(secondary, mode + ['--right-of', primary])
    if 'mirror' in config:
        xrandr(config['mirror'], mode + ['--same-as', primary])
    if 'unused' in config:
        for screen in config['unused']:
            xrandr(screen, ['--off'])
    if 'autolock' in config:
        run(['xautolock', '-enable' if config['autolock'] else '-disable'], check=True)


def main():
    """Program entry point.
    """
    args = parse_cmdline()

    with open(expanduser('~/.config/workstation_setups.toml'), encoding='utf-8') as config_file:
        config = load(config_file)
    try:
        config = config[gethostname()]
    except KeyError:
        print('Could not find current machine hostname in config')
        sys_exit(-1)
    try:
        config = config[args.location]
    except KeyError:
        print('Could not find specified location in config')
        sys_exit(-2)

    setup_screens(config['screens'], args.flip)
    if 'peripherals' in config:
        setup_peripherals(config['peripherals'])
    if 'commands' in config:
        for cmd in config['commands']:
            Popen(cmd) # pylint: disable=consider-using-with
    run('awesome-client', input='awesome.restart()', text=True, check=False)


if __name__ == "__main__":
    main()
