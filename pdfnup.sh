#!/bin/sh
# this script puts multiple pdf pages to one sheet for printing
# @author Sebastian Neuser

if [ $# -lt 2 ] || [ `expr length $2` -gt 2 ]
then
	echo "usage: `basename $0` <filename> <pages> [psnup args] [landscape] [separate]"
	echo "\tpages: number of pages per side"
	echo "\tpsnup args: program parameters for psnup"
	echo "\tlandscape: pages are in landscape format, not portrait"
	echo "\tseparate: create even and odd side pdfs instead of whole"
	echo
	echo "examples:"
	echo "\tpdfnup.sh print.pdf 2 separate"
	echo "\tpdfnup.sh resize.pdf 8 \"-H7.5cm -W11cm -pA4\""
	echo "\tpdfnup.sh in.pdf 4 -q (same as: pdfnup.sh in.pdf 4 landscape)"
	exit
fi
if [ ! -f /usr/bin/pdf2ps ] || [ ! -f /usr/bin/psnup ] ||
   [ ! -f /usr/bin/ps2pdf ] || [ ! -f /usr/bin/pdftk ]
then
	echo "*** sorry, you must have pdf2ps, psnup, ps2pdf and pdftk installed!"
	exit
fi

file=`echo $1 | cut -d "." -f 1`
args=`echo $@ | cut -d " " -f 1 --complement`
psnupargs=`echo $3 | sed -e s/landscape//g -e s/separate//g`

if [ $2 -gt 1 ]
then
	echo "pdf2ps $1"
	pdf2ps $1

	if [ `expr match "$args" .*landscape.*` -gt 0 ]
	then
		echo "psnup -q -f $psnupargs -$2 $file.ps $file-$2to1.ps"
		psnup -q -f $psnupargs -$2 $file.ps $file-$2to1.ps
	else
		echo "psnup -q $psnupargs -$2 $file.ps $file-$2to1.ps"
		psnup -q $psnupargs -$2 $file.ps $file-$2to1.ps
	fi

	echo "ps2pdf $file-$2to1.ps"
	ps2pdf $file-$2to1.ps
	echo "rm $file.ps $file-$2to1.ps"
	rm $file.ps $file-$2to1.ps
else
	echo "cp $1 $file-1to1.pdf"
	cp $1 $file-1to1.pdf
fi

if [ `expr match "$args" .*separate.*` -gt 0 ]
then
	echo "pdftk $file-$2to1.pdf cat 1-endodd output $file-$2to1-odd.pdf"
	pdftk $file-$2to1.pdf cat 1-endodd output $file-$2to1-odd.pdf
	echo "pdftk $file-$2to1.pdf cat end-1even output $file-$2to1-even.pdf"
	pdftk $file-$2to1.pdf cat end-1even output $file-$2to1-even.pdf
	echo "rm $file-$2to1.pdf"
	rm $file-$2to1.pdf
fi
