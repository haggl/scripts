#!/bin/sh
if [ $# -lt 2 ]
then
	echo "usage: $0 <minutes> <message>"
	exit
fi

x=`expr $1 + 1`
for i in `seq 1 $1`
do
	echo `expr $x - $i` minutes left...
	sleep 60
done

xmessage $2!
