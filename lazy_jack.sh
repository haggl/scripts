#!/bin/sh
# JACK / Pulse / REAPER startup script for lazy people.


# ---  V A R I A B L E S  --- #

JACK_SOUNDCARD_DEVICE=''
RESTART_DELAY=3

SCRIPT_NAME=$(basename "$0")



# ---  S U B R O U T I N E S  --- #

message() {
    test "$QUIET" || echo "$SCRIPT_NAME: $1"
}

print_help_message_and_exit() {
    cat << EOM
usage: $SCRIPT_NAME [-h]
       $SCRIPT_NAME [-C | -w <sec>] [-P] [-d <name>] [-m <name>] [-q] [-r] [<src:snk> ...]

JACK / Pulse / REAPER startup script for lazy people.

options:
  -C        do not clean up old session before starting
  -P        do not plug things together automatically
  -c        clean up only: kill JACK processes, start Pulse audio
  -d name   main audio hardware ALSA device name (default: ask)
  -h        show this help message
  -k        wait for [Enter] and kill JACK daemon and GUI afterwards
  -m name   device name of a microphone to connect Pulse audio JACK source
  -q        disable log messages
  -r        start and hook up REAPER DAW
  -w sec    seconds to wait between cleanup and startup

arguments:
  src:snk   MIDI devices to connect: src output will be connected to snk input
EOM
    exit
}

wait_for_port() {
    while true; do
        jack_lsp | grep -q "$1" && return;
        sleep 0.2
    done
}



# ---  M A I N   S C R I P T  --- #

while getopts "CPcd:hkm:qrw:" option; do
    case $option in
        C) DIRTY=yes ;;
        P) SKIP_PLUGGING=yes ;;
        c) CLEANUP_ONLY=yes ;;
        d) JACK_SOUNDCARD_DEVICE=$OPTARG ;;
        k) CLEANUP_JACK=yes ;;
        m) JACK_MICROPHONE_DEVICE=$OPTARG ;;
        q) QUIET=yes ;;
        r) REAPER=yes ;;
        w) RESTART_DELAY=$OPTARG ;;
        *) print_help_message_and_exit
    esac
done
shift $((OPTIND - 1))

# Clean up previous session
if [ -z "$DIRTY" ]; then
    message 'Cleaning up previous session'
    systemctl --user stop pulseaudio.socket
    systemctl --user stop pulseaudio.service
    pkill pavucontrol
    pkill jackd
    pkill qjackctl
    message 'Waiting for processes to close'
    for time_left in $(seq "$RESTART_DELAY" | sort -nr); do
        message "$time_left"
        sleep 1
    done
fi

if [ "$CLEANUP_ONLY" ]; then
    systemctl --user start pulseaudio.socket
    exit
fi

if [ -z "$JACK_SOUNDCARD_DEVICE" ]; then
    CARDS=$(grep '\[.*\]' /proc/asound/cards | sed 's#\s*\(\w*\)\s*\[\(\w*\)\s*\].*#\1: \2#')
    # Ask which sound card to use if there is more than one
    if [ "$(echo "$CARDS" | wc -l)" -gt 1 ]; then
        echo Please select a soundcard for JACK:
        echo "$CARDS"
        read -r JACK_SOUNDCARD_DEVICE
    else
        JACK_SOUNDCARD_DEVICE=${CARDS%:*}
    fi
fi

# Start JACK daemon and QjackCtl
message 'Starting JACK daemon, ALSA <-> JACK MIDI bridge and control GUI'
jackd -d alsa -d "hw:$JACK_SOUNDCARD_DEVICE" &
sleep 1
a2jmidid -e &
qjackctl &

# Configure camera as additional JACK input device
if [ "$JACK_MICROPHONE_DEVICE" ]; then
    message 'Configuring microphone JACK input'
    alsa_in -d "hw:$JACK_MICROPHONE_DEVICE" -c 1 &
    wait_for_port 'alsa_in:capture_1'
fi

# Start Pulse audio socket and pavucontrol
message 'Starting Pulse audio service and control GUI'
systemctl --user start pulseaudio.socket
pavucontrol &

# Load Pulse <-> JACK sink/source
message 'Loading Pulse JACK sink/source'
pactl load-module module-jack-sink
pactl load-module module-jack-source
pacmd set-default-sink jack_out

# If requested, start REAPER
if [ "$REAPER" ]; then
    reaper &
fi

# Plug stuff
if [ "$SKIP_PLUGGING" ]; then
    return
fi
if [ "$JACK_MICROPHONE_DEVICE" ]; then
    jack_connect 'alsa_in:capture_1' 'PulseAudio JACK Source:front-left'
    jack_connect 'alsa_in:capture_1' 'PulseAudio JACK Source:front-right'
fi
if [ "$REAPER" ]; then
    wait_for_port 'REAPER:out1'
    jack_connect 'REAPER:out1' 'PulseAudio JACK Source:front-left'
    jack_connect 'REAPER:out2' 'PulseAudio JACK Source:front-right'
fi
while [ "$1" ]; do
    src="${1%:*}"
    snk="${1#*:}"
    echo "Connecting $src -> $snk"
    src=$(jack_lsp | grep capture | grep "$src" | head -n1)
    snk=$(jack_lsp | grep playback | grep "$snk" | head -n1)
    jack_connect "$src" "$snk"
    shift
done

if [ "$CLEANUP_JACK" ]; then
    echo Press [Enter] to kill JACK
    read -r
    pkill a2jmidid
    pkill qjackctl
    pkill jackd
fi
