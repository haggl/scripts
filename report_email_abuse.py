#!/usr/bin/env python3
"""
Extract e-mail origin and find abuse contact.
"""

from argparse import ArgumentParser
from email import message_from_bytes
from functools import reduce
from itertools import count
from os import listdir, remove
from os.path import isdir, join
from re import findall, search
from subprocess import run
from sys import exit as sysexit
from textwrap import dedent

from eml_parser.eml_parser import decode_email_b


MESSAGE_TEMPLATE = dedent("""\
    Hello,

    we recently received Spam mail from an IP address in your range: {}
    Please look into it - you can find corresponding headers below.

    Thank you!



    -------- Forwarded Message --------
""")


def determine_recipient(address):
    """
    Determines the abuse contact e-mail address for an IP address.
    """
    def filter_abuse_email_addresses(results, string):
        if search(r'[Aa]buse', string):
            for addr in findall(r'[-_\.a-zA-Z0-9]+@[^ ]+\.[-_\.a-zA-Z0-9]+', string):
                results.append(addr)
        return results
    record = run(['whois', address], capture_output=True, text=True, errors='replace').stdout
    contacts = list(set(reduce(filter_abuse_email_addresses, record.split('\n'), [])))
    if not contacts:
        print(f'No abuse contacts for {address} found')
        return None
    if len(contacts) == 1:
        return contacts[0]
    print(f'Found multiple abuse contacts for message from {address}:')
    default_selection = len(contacts)
    for choice in zip(count(1), contacts):
        print(f'[{choice[0]}] {choice[1]}')
    selection = input(f'Selection ("s" to skip) [{default_selection}]: ')
    if not selection:
        selection = default_selection
    elif not selection.isdigit():
        print('Skipped')
        return None
    return contacts[int(selection) - 1]


def extract_headers(raw):
    """
    Extracts all e-mail headers from a message.
    """
    message = message_from_bytes(raw).as_string().split('\n')
    headers = []
    for line in message:
        if line.startswith('--') or line.startswith('Content-Type: '):
            break
        if line:
            headers.append(line)
    return '\n'.join(headers)


def generate_message(file_path):
    """
    Generates an email to the abuse contact for an e-mail and opens it in Thunderbird.
    """
    with open(file_path, 'rb') as mail_file:
        raw = mail_file.read()
    mail = decode_email_b(raw, include_attachment_data=True)
    if 'received_ip' not in mail['header']:
        print('No source IP address found')
        return False
    source = mail['header']['received_ip'][0]
    print('Origin is {}'.format(source))
    recipient = determine_recipient(source)
    if not recipient:
        return False
    subject = 'Fwd: ' + mail['header']['subject']
    body = MESSAGE_TEMPLATE.format(source) + extract_headers(raw)
    print(f'Generating message to abuse contact of {source}: {recipient}')
    run(['thunderbird', '-compose', f"to='{recipient}',subject='{subject}',body='{body}'"])
    return True


def parse_cmdline():
    """Parses command line arguments given to the script.

    Returns:
        the command line arguments parsed by argparse
    """
    parser = ArgumentParser(description=globals()['__doc__'])
    parser.add_argument('path', help='An e-mail file (.eml) or folder')
    return parser.parse_args()


def main():
    """Program entry point.
    """
    args = parse_cmdline()

    def process(path):
        if isdir(path):
            for entry in listdir(path):
                print(f'Processing file {entry}')
                process(join(path, entry))
            return
        command = ['file', '--brief', '--mime-type', path]
        mime_type = run(command, capture_output=True, text=True).stdout.strip()
        if mime_type in ['message/rfc822', 'text/html', 'text/plain']:
            try:
                success = generate_message(path)
                aye = 'Y' if success else 'y'
                ney = 'n' if success else 'N'
                choice = input(f'Delete file? [{aye}/{ney}]: ')
                if success and not choice or choice in ['y', 'Y', 'yes', 'Yes']:
                    remove(path)
            except KeyboardInterrupt:
                sysexit(0)
    process(args.path)


if __name__ == "__main__":
    main()
