#!/bin/sh
# this script extracts and crops a single page or image from a pdf
# @author Sebastian Neuser


# check for availability of necessary programs
if [ -z "`which pdfcrop`" -o -z "`which mutool`" -o -z "`which mudraw`" -o -z "`which feh`" ]; then
	echo "*** You must have pdfcrop, mutool, mudraw and feh installed!"
	exit
fi

# check for correct argument vector
if [ $# -lt 3 ]; then
	echo "usage: `basename $0` <input> <page> <output>"
    echo "\t<input>   input PDF file path"
    echo "\t<page>    page number of the input PDF containing the desired image / area"
    echo "\t<output>  output file path without file ending"
	exit
fi

# parse arguments
inputPath=$1
inputPage=$2
outputPath=$3

# check the input file
numberOfPages=`mutool info "$inputPath" | grep Pages | cut -d' ' -f2`
if [ $? -ne 0 -o $numberOfPages -lt $inputPage ]; then
    echo "$inputPath is not a readable PDF file or does not have a page #$inputPage!"
    exit
fi

# generate temporary files
EXTRACTED_PDF=`mktemp XXXXXX.pdf`
CROPPED_PDF=`mktemp XXXXXX.pdf`
RENDERED_PNG=`mktemp XXXXXX.png`

# extract target page
mutool clean -g -l "$inputPath" $EXTRACTED_PDF $inputPage

# check if there are images in the page
echo "Checking for embedded images..."
if [ -n "`mutool info $EXTRACTED_PDF | grep Images`" ]; then
    # extract and show them
    echo "Extracting embedded images..."
    images=`mutool extract $EXTRACTED_PDF 2> /dev/null | grep image | cut -d' ' -f3`
    fonts=`mutool extract $EXTRACTED_PDF 2> /dev/null | grep font | cut -d' ' -f3`

    # show images one by one
    for file in $images; do
        feh $file &
        pid=$!
        echo -n "Is this the image you want to crop? [y/N]: "
        read answer
        # if we have found the desired image, copy it to the output path
        if [ -n "`echo $answer | grep -i 'y'`" ]; then
            found=1
            ending=`echo $file | sed 's/.*\.\(.*\)/\1/'`
            cp $file "$outputPath.$ending"
            break;
        fi
        kill $pid
    done

    # clean up
    echo "Cleaning up..."
    kill $pid
    rm $fonts $images

    # exit if the image was found
    if [ -n "$found" ]; then
        rm $EXTRACTED_PDF $CROPPED_PDF $RENDERED_PNG
        exit
    fi
fi

# initial crop
echo "Cropping..."
pdfcrop $EXTRACTED_PDF $EXTRACTED_PDF > /dev/null

# open preview
echo "Creating preview..."
cp $EXTRACTED_PDF $CROPPED_PDF
mudraw -o $RENDERED_PNG $EXTRACTED_PDF
size=`feh -L %wx%h 2> /dev/null`
feh -Z -g $size -R 1 $RENDERED_PNG &
pid=$!

# determine margins
echo "If you want to crop further, please specify margins:"
left=
top=
right=
bottom=
while true
do
    unset change;

	echo -n "left [$left]: "
	read margin
	if [ -n "$margin" -a $margin -eq $margin ] 2> /dev/null; then
		change="left"
		left=$margin
	fi

	echo -n "top [$top]: "
	read margin
	if [ -n "$margin" -a $margin -eq $margin ] 2> /dev/null; then
		change="top"
		top=$margin
	fi

	echo -n "right [$right]: "
	read margin
	if [ -n "$margin" -a $margin -eq $margin ] 2> /dev/null; then
		change="right"
		right=$margin
	fi

	echo -n "bottom [$bottom]: "
	read margin
	if [ -n "$margin" -a $margin -eq $margin ] 2> /dev/null; then
		change="bottom"
		bottom=$margin
	fi

    if [ -z $change ]; then
        break;
    fi

    echo "Cropping..."
	pdfcrop --margins "-$left -$top -$right -$bottom" $EXTRACTED_PDF $CROPPED_PDF > /dev/null
    mudraw -o $RENDERED_PNG $CROPPED_PDF
    echo "If you want to crop further, please change margins:"
done

echo "Finishing..."

# rename temporary file
mv $CROPPED_PDF "$outputPath.pdf"

# clean up
rm $EXTRACTED_PDF $RENDERED_PNG
kill $pid
