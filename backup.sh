#!/bin/sh
# vim: expandtab tabstop=4 shiftwidth=4
# Synchronizes user data with a remote host or a storage device or config files with a storage device, automatically mounting it if necessary.
# @author Sebastian Neuser


# ---  S E T T I N G S  --- #
# directories to sync
DIRLIST="dokumente musik public_html repositories/html software tmp"
DEFAULT_DEVICE="backup"

# predefine rsync commands
SOFTSYNC="rsync -auv"
HARDSYNC="rsync -auv --delete"
EXCLUDE="--exclude=.git"



# ---  S U B R O U T I N E S  --- #
auto_mount_medium() {
    device=$1
    if [ -z "`df | grep /mnt/$device`" ]
    then
        echo "*** the medium is not mounted!"
        vault.sh -Odn $device
        # double check
        if [ -z "`df | grep /mnt/$device`" ]; then
            echo "*** the medium could not be mounted!"
            exit
        fi
        automounted=1
    fi
}
auto_unmount_medium() {
    device=$1
    if [ -n "$automounted" ]
    then
        echo "*** close vault? [Y/n]: "
        read close
        if [ -z "$close" -o -n "`echo $close | grep -i y`" ]
        then
            vault.sh -Cdn $1
        fi
    fi
}

backup_configs() {
    backup=$1

    # mount the medium if necessary
    auto_mount_medium $backup

    # synchronize /etc/
    echo -e "\n\nbacking up configs in '/etc/'..."
    sudo $HARDSYNC /etc /mnt/$backup/

    # synchronize dotfiles in user's $HOME
    echo -e "\n\nbacking up dotfiles in '$HOME'..."

    # create 'home' directory on the backup medium if it doesn't exist
    if [ ! -d /mnt/$backup/home ]; then
        mkdir -p /mnt/$backup/home
    fi

    # find and synchronize dotfiles
    for file in `find ~ -maxdepth 1 -name ".*" | sort`
    do
        $HARDSYNC $file /mnt/$backup/home/
    done

    # ask if the medium should be unmounted
    auto_unmount_medium $backup
}

backup_files() {
    backup=$1

    # mount the medium if necessary
    auto_mount_medium $backup

    # synchronize specified files
    for dir in $DIRLIST
    do
        echo -e "\n\n*** backing up '$dir'..."

        # create directory on the backup medium if it doesn't exist
        if [ ! -d /mnt/$backup/home/$dir/ ]; then
            mkdir -p /mnt/$backup/home/$dir/
        fi

        $HARDSYNC ~/$dir/ /mnt/$backup/home/$dir/
    done

    # ask if the medium should be unmounted
    auto_unmount_medium $backup
}

print_usage () {
    echo "usage: $0 [-b [device] | -c [device] | -m [device] | -s <host>]"
    echo
    echo "options:"
    echo -e "  -b\tback up user files"
    echo -e "  -c\tback up config files (user and system)"
    echo -e "  -m\tmount the backup medium"
    echo -e "  -s\tsynchronize user files with <host>"
    echo -e "  -u\tunmount the backup medium"
    echo
    echo "If [device] is not specified, the default \"$DEFAULT_DEVICE\" is used."
    echo "If the device is not present, the script will attempt to open it via vault.sh."
    echo "For that purpose vault.sh must be in the \$PATH."
    echo
    echo "Files will be copied to /mnt/<device>/."
}

synchronize_host() {
    host=$1
    if [ -n "$host" ]; then
        for dir in $DIRLIST
        do
            # get a list of files that would be deleted in a hard sync
            echo -e "\n\n*** syncing '$dir'..."
            FILES=`$HARDSYNC $EXCLUDE --dry-run ~/$dir/ $host:~/$dir/ | grep deleting`

            # ask if a soft sync should be performed to preserve obsolete files
            if [ -n "$FILES" ]
            then
                echo $FILES | sed "s/deleting /\n/g"
                echo -e "\n\n*** really delete these files? [Y/n]"
                read DEL
            fi

            # perform sync
            if [ -z "$DEL" -o -n "`echo $DEL | grep -i y`" ]
            then
                $HARDSYNC $EXCLUDE ~/$dir/ $host:$dir/
            else
                $SOFTSYNC $EXCLUDE ~/$dir/ $host:$dir/
            fi
        done
    else
        print_usage
        exit;
    fi
}



# ---  M A I N   S C R I P T  --- #

# check for necessary tools
if [ -z "`which rsync`" ]; then
    echo -e "\n\n*** could not find rsync!"
    exit
fi
if [ -z "`which vault.sh`" ]; then
    echo -e "\n\n*** could not find vault.sh!"
    exit
fi

# set the device if it was specified, otherwise use the default
if [ -n "$2" ]
then
    device=$2
else
    device=$DEFAULT_DEVICE
fi

# switch action
case "$1" in
    "-b") backup_files $device ;;
    "-c") backup_configs $device ;;
    "-m") vault.sh -Odn $device ;;
    "-s") synchronize_host $2 ;;
    "-u") vault.sh -Cdn $device ;;
    *)    print_usage
esac
