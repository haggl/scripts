#!/bin/sh -e
# A script that handles LUKS crypto devices and containers.
# @author Sebastian Neuser

# ---  S E T T I N G S --- #
DEFAULT_DEVICE="vault"
CIPHER="aes-cbc-essiv:sha256"
HASH="sha256"
KEY_SIZE="256"


# ---  S U B R O U T I N E S  --- #
clean_up() {
    sudo cryptsetup status "$name" >/dev/null && close_luks_partition

    # Delete loop device if in container mode
    if [ -n "$file_name" ]; then
        delete_loop_device
    fi
}

close_luks_partition() {
    echo "Closing LUKS partition $name"
    sudo cryptsetup luksClose "$name" \
        || { echo "Could not close $name!"; exit 2; }
}

create_filesystem() {
    echo "Creating file system"
    sudo mkfs.btrfs /dev/mapper/"$name" \
        || { echo "File system creation failed!"; exit 2; }
    # Wait for file system actions to complete
    sleep 4
    sync
}

create_luks_partition() {
    echo "Creating LUKS partition on $1"
    sudo cryptsetup --cipher=$CIPHER --hash=$HASH --key-size=$KEY_SIZE --use-random luksFormat "$1" \
        || { echo "Initialization of $1 failed!"; clean_up; exit 2; }
}

delete_loop_device() {
    echo "Deleting loop device"
    if [ -n "$loop_device" ]; then
        sudo losetup -d "$loop_device" \
            || { echo "Failed to delete $loop_device!"; exit 2; }
    else
        sudo losetup -d "$(sudo losetup -j "$file_name" | cut -d: -f1)" \
            || { echo "Failed to delete loop device!"; exit 2; }
    fi
}

extract_udev_info() {
    info=$(udevadm info --name="$device" --attribute-walk)
    echo "$info" | grep "$1" | head -n1 | cut -d\" -f2
}

mount_luks_partition() {
    echo "Mounting filesystem /dev/mapper/$name on $mountpoint"
    sudo mount "/dev/mapper/$name" "$mountpoint" \
        || { echo "Mounting of /dev/mapper/$name failed!"; clean_up; exit 2; }
}

open_luks_partition() {
    echo "Opening LUKS partition $1"
    sudo cryptsetup luksOpen "$1" "$name" \
        || { echo "Opening of $1 failed!"; clean_up; exit 2; }
}

print_help_and_exit() {
    script_name=$(basename "$0")
    echo "usage: $script_name  -O  <-c | -d | -r> [-m <mountpoint>] [-n <name>]"
    echo "       $script_name  -C  <-c | -d | -r> [-m <mountpoint>] [-n <name>]"
    echo "       $script_name  -I  <-c | -d>      [-m <mountpoint>] [-n <name>] <size / device>"
    echo "       $script_name  -D                                   [-n <name>] <device>"
    echo "       $script_name  -M                                   [-n <name>]"
    echo "       $script_name [-h]"
    echo
    echo "actions:"
    echo "  -O    Open and mount a LUKS container, device or RAID."
    echo "  -C    Unmount and close a LUKS container, device or RAID."
    echo "  -I    Initialize a LUKS container with the specified size (in MiB) or a LUKS device and create a file system on it."
    echo "  -D    Setup udev rules for the specified device."
    echo "  -M    Create mount point."
    echo
    echo "options:"
    echo "  -c          Operate on a LUKS container"
    echo "  -d          Operate on a LUKS device"
    echo "  -r          Operate on a RAID of two LUKS encrypted devices named <name>0 and <name1>"
    echo "  -m <mount>  Destination mount point. If omitted, /mnt/<name> is assumed."
    echo "  -n <name>   Device node name in /dev or filename. This name is also used in the device mapper."
    echo "              If omitted, the default $DEFAULT_DEVICE is assumed."
    exit 1
}

setup_loop_device() {
    echo "Setting up loop device"
    loop_device=$(sudo losetup -f)
    sudo losetup "$loop_device" "$file_name" \
        || { echo "Failed to setup $loop_device!"; exit 2; }
}

unmount_filesystem() {
    echo "Unmounting /dev/mapper/$name"
    sudo umount "/dev/mapper/$name" \
        || { echo "Unmounting of /dev/mapper/$name failed!"; exit 2; }
}

write_random_data() {
    echo "Filling $1 with $2 MiB of random data "
    dd if=/dev/urandom | pv -s "$2m" | sudo dd of="$1" bs=1M count="$2" iflag=fullblock 2>/dev/null \
        || { echo "Writing of random data failed!"; exit 2; }
}


# ---  M A I N   S C R I P T  --- #
# Parse options
while getopts "CDIMOcdm:n:r" option; do
    case $option in
    C)
        action="close"
        ;;

    I)
        action="initialize"
        ;;

    O)
        action="open"
        ;;

    D)
        action="setup_udev_rule"
        ;;

    M)
        action="setup_mount_point"
        ;;

    c)
        mode="container"
        ;;

    d)
        mode="device"
        ;;

    m)
        mountpoint=$OPTARG
        ;;

    n)
        name=$OPTARG
        ;;

    r)
        mode="raid"
        ;;

    *)
        print_help_and_exit
        ;;
    esac
done
shift $(( OPTIND - 1 ))

# Abort if no options were specified
if [ $OPTIND -eq 1 ]; then
    print_help_and_exit
fi

# Set default name if not specified
name=${name:-$DEFAULT_DEVICE}

# Post-process name for container management
if [ "$mode" = "container" ]; then
    file_name=$name
    name=$(basename "$name")
fi

# Set default mount point if it wasn't specified
mountpoint=${mountpoint:-/mnt/$name}

# Set container size
if [ "$action" = "initialize" ] && [ "$mode" = "container" ]; then
    size=$1
fi

# Set device path
if [ "$action" = "setup_udev_rule" ]; then
    device=$1
fi

case ${action}_${mode} in
    close_container)
        unmount_filesystem
        close_luks_partition
        delete_loop_device
        ;;

    close_device)
        unmount_filesystem
        close_luks_partition
        ;;

    close_raid)
        name=${name}0
        unmount_filesystem
        close_luks_partition
        name=${name%0}1
        close_luks_partition
        ;;

    initialize_container)
        write_random_data "$file_name" "$size"
        setup_loop_device
        create_luks_partition "$loop_device"
        open_luks_partition "$loop_device"
        create_filesystem
        close_luks_partition
        delete_loop_device
        ;;

    initialize_device)
        echo "Calculating device size in MiB"
        size=$(( $(sudo blockdev --getsize64 "/dev/$name") / (1024*1024) )) \
            || { echo "Failed to determine device size!"; exit 2; }
        write_random_data "/dev/$name" "$size"
        create_luks_partition "/dev/$name"
        open_luks_partition "/dev/$name"
        create_filesystem
        close_luks_partition
        ;;

    open_container)
        setup_loop_device
        open_luks_partition "$loop_device"
        mount_luks_partition
        ;;

    open_device)
        open_luks_partition "/dev/$name"
        mount_luks_partition
        ;;

    open_raid)
        name=${name}0
        open_luks_partition "/dev/$name"
        name=${name%0}1
        open_luks_partition "/dev/$name"
        name=${name%1}0
        mount_luks_partition
        ;;

    setup_mount_point_)
        echo "Creating mount point"
        sudo mkdir -p "/mnt/$name"
        ;;

    setup_udev_rule_)
        echo "Extracting product name and serial number from of $device from udevadm"
        product=$(extract_udev_info product)
        serial=$(extract_udev_info serial)

        echo "Setting up udev udev rules"
        rules=$(mktemp)
        printf 'SUBSYSTEM=="block", ATTRS{product}=="%s", ATTRS{serial}=="%s", SYMLINK+="%s"\n' \
               "$product" "$serial" "$name" > "$rules"
        sudo mv "$rules" "/etc/udev/rules.d/99-$name.rules"
        ;;

    *)
        print_help_and_exit
        ;;
esac
