#!/usr/bin/perl
# universal latex perl script: automates many common tasks with and around the latex command
# @author Sebastian Neuser

use strict;
#use warnings;
use Getopt::Std;
use File::Basename;


# --------- C O M M A N D   L I N E   A R G U M E N T S --------- #
my %opts;
getopts("ABCNabceglpv" , \%opts);


# ------------------------- C H E C K S ------------------------- #
# check command line parameters
if ($#ARGV < 0 and !$opts{"A"} and !$opts{"N"}) { printHelp(); }


# ---------------------- V A R I A B L E S ---------------------- #
# declare and initialize global variables
my $file = $ARGV[$#ARGV];
if ($opts{"A"} or $opts{"N"}) # try to find the master document
{
    $file = `grep -l hitex:master *.tex`;
    chomp($file);
    $file =~ s/\.tex.*//;

    if ($file eq "") {
        print "*** hitex:master could not be found!\n";
        exit
    }

    if ($opts{"N"}) {
        print $file;
        exit
    }

    $_ = `grep hitex:master *.tex`;
    chomp;
    s/.*hitex:master\s*//;
    if (/B/) {
        $opts{"B"} = 1;
    }
    if (/e/) {
        $opts{"e"} = 1;
    }
    if (/l/) {
        $opts{"l"} = 1;
    }
}
my $latex = $opts{"l"} ? "latex" : "pdflatex"; # if -l option was given, use 'latex', else 'pdflatex'
if ($opts{"e"}) { $latex .= " -shell-escape" }
my $cmdline = "$latex $file &&";
my $bibparse = $opts{"B"} ? "biber" : "bibtex"; # if -B option was given, use 'biber', else 'bibtex'


# ------------------- M A I N   P R O G R A M ------------------- #
unless (-e "$file.tex")
{
    print "***$file.tex does not exist!\n";
    exit;
}
unless (-r "$file.tex")
{
    print "***unable to read $file.tex: operation not permitted!\n";
    exit;
}

if ($opts{"c"} or $opts{"C"}) # clean up
{
    opendir(DIR,".");
    my @files = readdir(DIR);
    closedir(DIR);

    my @texFiles;
    foreach (@files)
    {
        if (/.*\.tex/)
            { push @texFiles, $_ }
    }

    foreach (@files)
    {
        my $currentFile = $_;
        $currentFile =~ s/\.aux$//;
        if (/^$file.*/ and !/$file\.tex/ and !/$file\.pdf/ and !/$file\.bib/
        or  /\.aux$/ and grep(/$currentFile\.tex/, @texFiles))
            { unlink($_) }
    }

    if ($opts{"C"}) { exit }
}

if ($opts{"p"}) # create eps picture
{
    $cmdline = "latex $file && dvips -E $file.dvi -o $file.eps";
}
else # "normal" mode
{
    # build command line according to given options
    if ($opts{"a"})
        { $cmdline .= " $bibparse $file && makeglossaries $file && makeindex $file.nlo -s nomencl.ist -o $file.nls && $latex $file &&" }
    if ($opts{"b"})
        { $cmdline .= " $bibparse $file && $latex $file &&" }
    if ($opts{"g"})
        { $cmdline .= " makeglossaries $file && makeindex $file.nlo -s nomencl.ist -o $file.nls && $latex $file" }

    # append last latex-run and if -l option was given also dvips and ps2pdf
    $cmdline .= $opts{"l"} ? " $latex $file && dvips $file.dvi && ps2pdf $file.ps" : " $latex $file" ;
}

# append texcount command if desired
if ($opts{"v"})
    { $cmdline .= " && texcount -inc -total $file.tex"; }

execute();


# -------------------- S U B R O U T I N E S -------------------- #
sub execute
{
    system( $cmdline );
    print "***executed: $cmdline\n";
}

sub printHelp
{
    print "usage: " .basename($0). " [options] [<target>]\n";
    print "\n";
    print "target: name of the source file (without .tex)\n";
    print "\n";
    print "options:\n";
    print "    -A: search all tex sources for the comment 'hitex:master' and\n";
    print "        use the file containing the first occurrence as <target>\n";
    print "        some of the other options can be specified at the end of this line\n";
    print "    -B: use 'biber' instead of 'bibtex' for bibliography parsing\n";
    print "        this option can also be specified in the hitex:master comment line\n";
    print "    -N: just print out the basename of the file that contains the\n";
    print "        'hitex:master' comment\n";
    print "    -a: compile with bibtex (or biber) and glossaries\n";
    print "    -b: compile with bibtex (or biber)\n";
    print "    -c: delete old auxilliary files before compiling\n";
    print "    -C: just delete old auxilliary files\n";
    print "    -e: allow shell escapes (possibly insecure!)\n";
    print "        this option can also be specified in the hitex:master comment line\n";
    print "    -g: compile with glossaries\n";
    print "    -l: use long compilation method (latex > dvips > ps2pdf) instead of pdflatex\n";
    print "        this option can also be specified in the hitex:master comment line\n";
    print "    -p: compile a picture to eps (other options except -c  have no effect)\n";
    print "    -v: print word count after compilation\n";
    exit;
}
