#!/usr/bin/env python3
"""Inverts the mouse wheel scroll direction of an xinput pointer device.
"""

from subprocess import run
from sys import argv
from typing import List


def execute(cmd: List[str]) -> List[str]:
    """Executes a command and returns its stdout.

    Args:
        cmd:    The command to execute as a list of strings

    Returns:
        The executed command's stdoud as a list of strings.
    """
    stdout = run(cmd, check=True, capture_output=True, text=True).stdout
    return [line for line in stdout.split('\n') if len(line)]

def parse_device(dev: str) -> dict:
    """Parses an xinput line into a device dictionary.

    Args:
        dev     The xinput output line to parse

    Returns:
        A dictionary: {name, id}
    """
    columns = dev.split('\t')
    return {
        'name': columns[0].strip('⎜↳ '),
        'id': columns[1][3:]
    }


def main():
    """Program entry point.
    """
    devices = execute(['xinput', 'list'])
    def is_relevant(device: str):
        return 'slave  pointer' in device and 'XTEST' not in device
    pointers = [parse_device(dev) for dev in devices if is_relevant(dev)]
    if len(argv) < 2:
        for idx, dev in enumerate(pointers):
            print(f'{idx}: {dev["name"]}')
        selection = int(input('Choose a device: '))
    else:
        selection = [p['name'] for p in pointers].index(argv[1])
    execute([
        'xinput',
        'set-prop',
        pointers[selection]['id'],
        'libinput Natural Scrolling Enabled',
        '1'
    ])


if __name__ == "__main__":
    main()
