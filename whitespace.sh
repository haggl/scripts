#!/bin/sh
# This script deals with several common whitespace issues.
# @author Sebastian Neuser



# ---  V A R I A B L E S --- #

count=0
number_of_arguments=$#



# ---  S U B R O U T I N E S  --- #

check_number_of_arguments() {
    if [ $number_of_arguments -lt $1 ]; then
        print_help_message
        exit
    fi
}

convert_crlf_to_lf() {
    echo "*** converting line endings in directory $1"
    find $1 -type f -exec sed -i 's/$//' '{}' \;
}

print_help_message() {
    echo "usage: `basename $0` [-F | -X <regex>]"
    echo "       `basename $0` [-e <file ending> | -t <num> <file ending>]"
    echo "       `basename $0` [-l <directory>]"
    echo
    echo "options:"
    echo "\t-F:\tstrip whitespaces from file- and directory names"
    echo "\t-X:\tstrip given sed-regex from file- and directory names"
    echo "\t-e:\trecursively strip trailing whitespace from all files with the given ending"
    echo "\t-l:\trecursively convert DOS line endings to Unix line endings"
    echo "\t-t:\trecursively convert tabs to the given number of spaces in all file with the given ending"
}


strip_filename_whitespace() {
    echo "*** searching for whitespaces..."
    for file in *
    do
        newfile=`echo $file | sed s/[[:space:]]//g`
        if [ "$file" != $newfile ]
        then
            mv -iv "$file" $newfile
            count=`expr $count + 1`
        fi
    done
}

strip_filename_regex() {
    echo "*** searching for regex: /$1/"
    for file in *
    do
        newfile=`echo $file | sed s/$1//g`
        if [ "$file" != "$newfile" ]
        then
            mv -iv "$file" $newfile
            count=`expr $count + 1`
        fi
    done
}

translate_tabs_to_spaces() {
    echo "*** translating all tabs to $1 spaces in *.$2 files"
    for count in `seq $1`; do
        spaces="$spaces "
    done
    find . -name "*.$2" -exec sed -i "s/\\t/$spaces/g" "{}" \;
}

trim_whitespace() {
    echo "*** trimming whitespace in *.$1 files"
    find . -name "*.$1" -exec sed -i 's/\s*$//' "{}" \;
}



# ---  M A I N   S C R I P T  --- #

case "$1" in
    "-F")   strip_filename_whitespace ;;

    "-X")   check_number_of_arguments 2
            strip_filename_regex $2
            ;;

    "-l")   check_number_of_arguments 2
            convert_crlf_to_lf $2
            exit
            ;;

    "-e")   check_number_of_arguments 2
            trim_whitespace $2
            exit
            ;;

    "-t")   check_number_of_arguments 3
            translate_tabs_to_spaces $2 $3
            exit
            ;;

    *)      print_help_message
            exit
esac


# report number of matches
if [ $count -lt 1 ]
then
    echo "*** no match!"
elif [ $count -eq 1 ]
then
    echo "*** one match."
elif [ $count -gt 1 ]
then
    echo "*** $count matches."
fi
