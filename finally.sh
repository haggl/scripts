#!/bin/sh
# executes the given command and quietly shuts the server down afterwards
# @author haggl

# check if a command was specified
if [ $# -lt 1 ]
then
        echo "usage: $0 <command>"
	exit
fi

# disable beeps
sudo modprobe -r pcspkr

# execute the command and shut down
command="$1"
shift
bash -ic "$command '$*'"
if [ $? -eq 0 ]
then
	sudo shutdown -h 5
fi

# in case the shutdown is cancelled, reenable the bell
sudo modprobe pcspkr
